FROM jenkins/jenkins:lts

USER root

RUN apt-get update

RUN curl -sSL https://get.docker.com/ | sh

RUN groupmod -g 116 docker
RUN usermod -aG 116 jenkins

USER jenkins